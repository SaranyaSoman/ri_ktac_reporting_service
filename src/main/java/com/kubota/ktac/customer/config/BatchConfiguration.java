package com.kubota.ktac.customer.config;

import java.util.HashMap;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.data.MongoItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.kubota.ktac.customer.dto.Customer;
import com.kubota.ktac.customer.processor.CustomerDetailsProcessor;
import com.kubota.ktac.customer.writer.CustomerDetailsWriter;

import lombok.extern.slf4j.Slf4j;

@Configuration
@EnableBatchProcessing
public class BatchConfiguration {

	@Autowired
	public JobBuilderFactory jobBuilderFactory;
	
	@Autowired
	public StepBuilderFactory stepBuilderFactory;
	
	@Autowired
	public MongoTemplate mongoTemplate;
	
	@Bean
	public Job customerJob(Step customerStep) {
		return jobBuilderFactory.get("customerJob")
				.incrementer(new RunIdIncrementer()).flow(customerStep).end().build();
	}
	
	@Bean
	public Step customerStep() {
		return stepBuilderFactory.get("customerStep").<Customer, Customer>chunk(10).reader(reader())
				//.processor(processor())
				.writer(writer()).build();
		
	}
	
//	@Bean
//	public Step storeFileStep() {
//		return stepBuilderFactory.get("storeFileStep").chunk(10).writer(storeFileWriter()).
//	}
	
	@Bean
	public MongoItemReader<Customer> reader(){
		MongoItemReader<Customer> reader = new MongoItemReader<Customer>();
		reader.setTemplate(mongoTemplate);
		reader.setCollection("customer");
		reader.setQuery("{}");
		reader.setSort(new HashMap<String, Sort.Direction>(){{
			put("customerNumber", Direction.DESC);
		}});
		reader.setTargetType(Customer.class);
		return reader;
	}
	
	@Bean
	public CustomerDetailsProcessor processor(){
		return new CustomerDetailsProcessor();
	}
	
	@Bean
	public ItemWriter<Customer> writer(){
		return new CustomerDetailsWriter();
	}
	
//	@Bean
//	public File storeFileWriter(){
//		
//	}
}
