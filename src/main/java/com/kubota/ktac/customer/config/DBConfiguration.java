//package com.kubota.ktac.customer.config;
//
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.data.mongodb.MongoDatabaseFactory;
//import org.springframework.data.mongodb.core.MongoTemplate;
//import org.springframework.data.mongodb.core.SimpleMongoClientDatabaseFactory;
//
//import com.mongodb.client.MongoClient;
//import com.mongodb.client.MongoClients;
//
//@Configuration
//public class DBConfiguration {
//	
//	@Value("${database.uri}")
//	private String connectionURL;
//	
//	@Value("${database.name}")
//	private String databaseName;
//	
//	@Bean
//	public MongoClient mongoClient() {
//		return MongoClients.create(connectionURL);
//	}
//	
////	@Bean
////	public MongoTemplate mongoTemplate() {
////		return new MongoTemplate(mongoClient(), databaseName);
////	}
//
//	@Bean
//	public MongoDatabaseFactory mongoDbFactory() {
//		return new SimpleMongoClientDatabaseFactory(mongoClient(), databaseName);
//		
//	}
//	
//	@Bean
//	public MongoTemplate mongoTemplate() {
//		return new MongoTemplate(mongoDbFactory());
//	}
//	
//}
