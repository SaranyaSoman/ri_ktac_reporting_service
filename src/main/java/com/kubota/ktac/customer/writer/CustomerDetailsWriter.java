package com.kubota.ktac.customer.writer;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Value;

import com.azure.storage.blob.BlobClient;
import com.azure.storage.blob.BlobContainerClient;
import com.azure.storage.blob.BlobServiceClient;
import com.azure.storage.blob.BlobServiceClientBuilder;
import com.kubota.ktac.customer.dto.Customer;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

public class CustomerDetailsWriter implements ItemWriter<Customer>{
	
	@Value("${template.path}")
	private String templatePath;
	
	@Value("${logo.path}")
	private String logoPath;
	
	static String storageAccountUrl = "https://dkccsaea01.blob.core.windows.net";
	static String sasToken = "?sv=2019-10-10&ss=b&srt=sco&sp=rwdlacx&se=2021-01-01T00:45:18Z&st=2020-06-07T15:45:18Z&spr=https&sig=nyoVL7AbIeIPevWw5d2YaBo4luyWnbnh2ojWnzqOYS0%3D";

	static String filePath = "C:\\";

	@Override
	public void write(List<? extends Customer> customer) throws Exception {
		generatePDF(customer);
		
	}

	public void generatePDF(List<? extends Customer> customer) throws IOException {
		String fileName= "Customer_Report_"+ GetCurrentTimeStamp() +".pdf";
		String targetFileName = "Azure_"+fileName;
		try {
			JasperReport jasperReport = loadTemplate();
			Map<String, Object> parameters = parameter();
			JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(customer);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
			JasperExportManager.exportReportToPdfFile(jasperPrint, filePath+ fileName);
			String fileStored = storeFile("customer",fileName, targetFileName);
		} catch (JRException e) {
			e.printStackTrace();
		}
	}

	public String storeFile(String containerName, String sourceFileName, String targetFileName) {
		BlobServiceClient blobServiceClient = new BlobServiceClientBuilder().endpoint(storageAccountUrl)
				.sasToken(sasToken).buildClient();
		BlobContainerClient containerClient = createContainerIfNotExists(containerName, blobServiceClient);

		//String uploadFileName = FileUtil.appendTimeStampToUploadFilename(sourceFileName, targetFileName);
		System.out.println("Source: " + sourceFileName);
		System.out.println("Target: " + targetFileName);
		System.out.println("uploadFileName: " + targetFileName);

		BlobClient blobClient = containerClient.getBlobClient(targetFileName);
		System.out.println("\nUploading to Blob storage as blob:\n" + blobClient.getBlobUrl());

		blobClient.uploadFromFile(filePath + sourceFileName);
		return "File Upload SUCCESS!!!" + "\nContainer: " + containerName + "\nFile Location: "
				+ blobClient.getBlobUrl();
	}

	
	public static String GetCurrentTimeStamp() {
        SimpleDateFormat sdfDate = new SimpleDateFormat(
                "yyMMddHHmmssSS");
        Date now = new Date();
        String strDate = sdfDate.format(now);
        return strDate;
	}
	private Map<String, Object> parameter() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("createdBy", "Saranya Soman");
		map.put("logo", getClass().getResourceAsStream(logoPath));
		return map;
	}

	private JasperReport loadTemplate() throws JRException {
		InputStream inputStream = getClass().getResourceAsStream(templatePath);
		JasperDesign design = JRXmlLoader.load(inputStream);
		return JasperCompileManager.compileReport(design);
	}
	
	private BlobContainerClient createContainerIfNotExists(String containerName, BlobServiceClient blobServiceClient) {
		BlobContainerClient containerClient;
		if (blobServiceClient.getBlobContainerClient(containerName).exists()) {
			System.out.println("Container exists");
			containerClient = blobServiceClient.getBlobContainerClient(containerName);
		} else {
			System.out.println("Container created");
			containerClient = blobServiceClient.createBlobContainer(containerName);
		}
		return containerClient;
	}
	

}
