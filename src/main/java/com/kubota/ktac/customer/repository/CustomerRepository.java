package com.kubota.ktac.customer.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.kubota.ktac.customer.dto.Customer;

public interface CustomerRepository extends MongoRepository<Customer, Customer> {

}
