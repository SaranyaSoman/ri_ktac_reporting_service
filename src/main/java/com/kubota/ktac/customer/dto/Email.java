package com.kubota.ktac.customer.dto;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document(collection="email")
public class Email {
	
	private String emailType;
    
    private String emailAddress;

}
