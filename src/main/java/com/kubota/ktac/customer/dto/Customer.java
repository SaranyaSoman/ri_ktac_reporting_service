package com.kubota.ktac.customer.dto;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;



@Data
@Document(collection = "customer")
public class Customer {
	
	private int customerNumber;

	@NotNull
	private Integer bizGroupId;

	@NotNull
	private Integer taxId;

	@NotNull
	private String shortName;
	@NotNull
	private String firstName;

	@NotNull
	private String lastName;
	private String middleInitial;
	private String businessName;

	private String preferredLanguage;

	@NotNull
	private String source;
	private List<Address> addresses;
	private List<Phone> phones;
	private List<Email> emails;
	
	
	
		
	
}
