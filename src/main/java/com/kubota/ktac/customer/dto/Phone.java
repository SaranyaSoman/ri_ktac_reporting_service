package com.kubota.ktac.customer.dto;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document(collection="phone")
public class Phone {
	
	private String phoneType;
    private String phoneNumber;

}
