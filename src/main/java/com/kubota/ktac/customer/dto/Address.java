package com.kubota.ktac.customer.dto;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document(collection = "address")
public class Address {
	
	private String addressType;
    private String address1;
    private String address2;
    private String city;
    private String state;
    private String zipCode;
    private String delPt;

}
